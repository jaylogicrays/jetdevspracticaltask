import com.practical.homeworkmvvm.data.remote.login.response.LoginResponse
import com.practical.homeworkmvvm.data.remote.login.response.UserResponse
import com.practical.homeworkmvvm.domain.LoginUseCases
import com.practical.homeworkmvvm.ui.login.LoginViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @Mock
    lateinit var loginUseCases: LoginUseCases

    @Test
    fun `submitLoginDetail should emit login response`() {
        val userName = "username"
        val password = "password"
        val loginResponse = LoginResponse()
        Mockito.`when`(loginUseCases
            .submitUserData(Mockito.any()))
            .thenReturn(flowOf(loginResponse))

        val viewModel = LoginViewModel(loginUseCases)
        viewModel.submitLoginDetail(userName, password)
        assertEquals(true, viewModel.isLoading.value)
        assertEquals(loginResponse, runBlocking { viewModel.resultLiveData.first() })
        assertEquals(false, viewModel.isLoading.value)
    }

    @Test
      fun `insertUserData should insert user data`() {
        val userId = "user123"
        val userName = "username"
        val isDeleted = false

        val viewModel = LoginViewModel(loginUseCases)

        viewModel.insertUserData(userId, userName, isDeleted)

        Mockito.verify(loginUseCases).insertUserData(
            UserResponse(userId, userName, isDeleted)
        )
    }
}
