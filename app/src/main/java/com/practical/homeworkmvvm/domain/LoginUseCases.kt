package com.practical.homeworkmvvm.domain

import com.practical.homeworkmvvm.data.remote.login.response.LoginResponse
import com.practical.homeworkmvvm.data.remote.login.response.UserResponse
import kotlinx.coroutines.flow.Flow

interface LoginUseCases {
    fun submitUserData(hashMap: HashMap<String, Any>): Flow<LoginResponse>

    fun insertUserData(userResponse: UserResponse)
}