package com.practical.homeworkmvvm.domain

import com.practical.homeworkmvvm.data.remote.login.LoginRepository
import com.practical.homeworkmvvm.data.remote.login.response.LoginResponse
import com.practical.homeworkmvvm.data.remote.login.response.UserResponse
import kotlinx.coroutines.flow.Flow

class LoginUserUseCasesImpl(
    private val loginRepository: LoginRepository
) : LoginUseCases {
    override fun submitUserData(hashMap: HashMap<String, Any>): Flow<LoginResponse> {
        return loginRepository.getLoginData(hashMap)
    }

    override fun insertUserData(userResponse: UserResponse) {
        loginRepository.insertUserData(userResponse)
    }
}