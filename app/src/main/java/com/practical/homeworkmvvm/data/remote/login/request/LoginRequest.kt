package com.practical.homeworkmvvm.data.remote.login.request

data class LoginRequest(
    val username: String = "",
    var password: String = ""
)
