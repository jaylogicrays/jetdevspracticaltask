package com.practical.homeworkmvvm.data.local.login

import androidx.room.Database
import androidx.room.RoomDatabase
import com.practical.homeworkmvvm.data.remote.login.response.UserResponse

@Database(entities = [UserResponse::class], version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {
    abstract val userDao: UserDao
}