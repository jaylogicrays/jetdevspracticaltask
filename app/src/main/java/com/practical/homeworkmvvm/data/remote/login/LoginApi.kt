package com.practical.homeworkmvvm.data.remote.login

import com.practical.homeworkmvvm.data.remote.login.response.LoginResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {

    @POST("login")
    fun getLoginResponse(@Body params: HashMap<String, Any>): Deferred<LoginResponse>
}