package com.practical.homeworkmvvm.data.remote.login

import com.practical.homeworkmvvm.data.remote.login.response.LoginResponse
import com.practical.homeworkmvvm.data.remote.login.response.UserResponse
import kotlinx.coroutines.flow.Flow

interface LoginRepository {

    fun getLoginData(params: HashMap<String, Any>): Flow<LoginResponse>

    fun insertUserData(userResponse: UserResponse)
}