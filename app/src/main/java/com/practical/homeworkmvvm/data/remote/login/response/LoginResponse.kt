package com.practical.homeworkmvvm.data.remote.login.response

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class LoginResponse(
    val errorCode: String? = "",
    val errorMessage: String? = "",
    val data: UserResponse? = null
)

@Entity(tableName = "User")
data class UserResponse(
    @PrimaryKey
    @ColumnInfo(name = "userId") var userId: String = "",
    @ColumnInfo(name = "userName") var userName: String = "",
    @ColumnInfo(name = "isDeleted") var isDeleted: Boolean = false
)
