package com.practical.homeworkmvvm.data.remote.login

import com.practical.homeworkmvvm.data.local.login.UserDao
import com.practical.homeworkmvvm.data.remote.login.response.UserResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class LoginDataRepository(
    private val loginApi: LoginApi,
    private var userDao: UserDao
) : LoginRepository {
    override fun getLoginData(params: HashMap<String, Any>) = flow {
        val response = loginApi.getLoginResponse(params).await()
        emit(response)
    }.flowOn(Dispatchers.IO)

    override fun insertUserData(userResponse: UserResponse) {
        userDao.insertUser(userResponse)
    }
}