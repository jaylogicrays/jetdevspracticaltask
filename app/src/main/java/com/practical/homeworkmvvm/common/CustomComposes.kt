package com.practical.homeworkmvvm.common

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.practical.homeworkmvvm.ui.theme.ButtonColor
import com.practical.homeworkmvvm.ui.theme.LightGrayColor

@Composable
fun VerticalSpacer(
    height: Int = 5,
    backgroundColor: Color = Color.White,
    horizontalPadding: Int = 0
) {
    Spacer(
        modifier = Modifier
            .height(height.dp)
            .fillMaxWidth()
            .padding(horizontal = horizontalPadding.dp)
            .background(backgroundColor)
    )
}

@Composable
fun HorizontalSpacer(
    width: Int = 5,
    backgroundColor: Color = Color.White,
    horizontalPadding: Int = 0
) {
    Spacer(
        modifier = Modifier
            .width(width.dp)
            .padding(horizontal = horizontalPadding.dp)
            .background(backgroundColor)
    )
}


@Composable
fun CustomTextField(
    modifier: Modifier,
    hintText: String = "",
    labelText: String = hintText,
    textFieldValue: String,
    onValueChanged: (text: String) -> Unit,
    onDoneClick: (text: String) -> Unit,
    isEnable: Boolean = true,
    errorText: Boolean = false,
    keyboardInputType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Next,
    singleLine: Boolean = true,
    maxLines: Int = 1,
    minLines: Int = 1,
    shape: Shape = RoundedCornerShape(2.dp)
) {
    val colors = TextFieldDefaults.colors(
        focusedContainerColor = Color.White,
        unfocusedContainerColor = Color.White,
        disabledContainerColor = Color.White,
        cursorColor = Color.Black,
        focusedIndicatorColor = Color.White,
        unfocusedIndicatorColor = Color.White,
        errorIndicatorColor = Color.Transparent,
        errorContainerColor = Color.White
    )

    TextField(
        value = textFieldValue,
        onValueChange = { onValueChanged(it) },
        modifier = modifier
            .fillMaxWidth()
            .border(1.dp, if (errorText) Color.Red else LightGrayColor, shape)
            .background(if (errorText) Color.White else Color.White),
        enabled = isEnable,
        textStyle = TextStyle(
            fontWeight = FontWeight(400),
            fontSize = 14.sp,
            color = Color.Black
        ),
        label = {
            Text(
                text = labelText,
                style = TextStyle(
                    fontWeight = FontWeight(500),
                    fontSize = 12.sp,
                    color = LightGrayColor
                )
            )
        },
        isError = errorText,
        keyboardActions = KeyboardActions(onDone = { onDoneClick }),
        keyboardOptions = KeyboardOptions(keyboardType = keyboardInputType, imeAction = imeAction),
        singleLine = singleLine,
        maxLines = maxLines,
        minLines = minLines,
        shape = shape,
        colors = colors
    )
}

@Composable
fun CustomButton(
    modifier: Modifier = Modifier,
    text: String,
    backgroundColor: ButtonColors = ButtonDefaults.buttonColors(ButtonColor),
    corner: Shape = RoundedCornerShape(5.dp),
    fontSize: Int = 14,
    fontColor: Color = Color.White,
    align: TextAlign = TextAlign.Center,
    onClick: (() -> Unit),
    enabled: Boolean = true
) {
    Button(
        onClick = onClick,
        modifier = modifier,
        elevation = ButtonDefaults.buttonElevation(1.dp),
        shape = corner,
        contentPadding = PaddingValues(horizontal = 17.dp),
        colors = backgroundColor,
        enabled = enabled
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = text,
                textAlign = align,
                modifier = Modifier.weight(1f),
                style = TextStyle(
                    fontSize = fontSize.sp,
                    color = fontColor,
                ),
            )
        }
    }
}

@Composable
fun CustomLoader(
    loaderColor: Color = ButtonColor
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxWidth()
            .height(55.dp)
    ) {
        CircularProgressIndicator(
            color = loaderColor,
            modifier = Modifier.requiredSize(40.dp)
        )
    }
}
