package com.practical.homeworkmvvm.ui.login

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.practical.homeworkmvvm.R
import com.practical.homeworkmvvm.common.CustomButton
import com.practical.homeworkmvvm.common.CustomLoader
import com.practical.homeworkmvvm.common.CustomTextField
import com.practical.homeworkmvvm.common.VerticalSpacer
import com.practical.homeworkmvvm.ui.theme.HomeWorkMVVMTheme
import com.practical.homeworkmvvm.utils.isEmailValid
import com.practical.homeworkmvvm.utils.isPasswordValid
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : ComponentActivity() {

    private val viewModel by viewModel<LoginViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HomeWorkMVVMTheme {
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    LoginCompose(loginViewModel = viewModel)
                }
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun LoginCompose(
    loginViewModel: LoginViewModel?
) {
    val context = LocalContext.current
    var userNameValue by remember { mutableStateOf("") }
    var passwordValue by remember { mutableStateOf("") }
    var isUserNameError by remember { mutableStateOf(false) }
    var isPasswordError by remember { mutableStateOf(false) }
    val keyboardController = LocalSoftwareKeyboardController.current
    val loginResponse by loginViewModel!!.resultLiveData.collectAsState()
    val isLoading by loginViewModel!!.isLoading.collectAsState()
    println("isLoading $isLoading")
    LaunchedEffect(key1 = loginResponse) {
        if (loginResponse != null) {
            when (loginResponse!!.errorCode) {
                "00" -> {
                    loginResponse!!.data?.apply {
                        loginViewModel!!.insertUserData(userId, userName, isDeleted)
                        Toast.makeText(context, context.getString(R.string.login_success), Toast.LENGTH_SHORT).show()
                        userNameValue = ""
                        passwordValue = ""
                    }
                }

                "01" -> {
                    Toast.makeText(context, "01 ${loginResponse!!.errorCode}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
            .padding(horizontal = 18.dp, vertical = 36.dp)

    ) {

        VerticalSpacer(height = 30)

        Image(
            modifier = Modifier
                .width(130.dp)
                .height(60.dp)
                .align(Alignment.CenterHorizontally),
            painter = painterResource(id = R.drawable.app_logo),
            contentDescription = "app_logo"
        )

        VerticalSpacer(height = 20)

        CustomTextField(
            textFieldValue = userNameValue,
            modifier = Modifier.padding(top = 10.dp),
            hintText = context.getString(R.string.enter_user_name),
            labelText = context.getString(R.string.username),
            keyboardInputType = KeyboardType.Text,
            imeAction = ImeAction.Next,
            onValueChanged = { text ->
                userNameValue = text
                isUserNameError = !isEmailValid(text)
            },
            errorText = isUserNameError,
            onDoneClick = {}
        )

        CustomTextField(
            textFieldValue = passwordValue,
            modifier = Modifier.padding(top = 10.dp),
            hintText = context.getString(R.string.enter_user_password),
            labelText = context.getString(R.string.password),
            keyboardInputType = KeyboardType.Text,
            imeAction = ImeAction.Done,
            onValueChanged = { text ->
                passwordValue = text
                isPasswordError = !isPasswordValid(text)
            },
            errorText = isPasswordError,
            onDoneClick = {
                keyboardController?.hide()
                if (isInternetAvailable(context)) {
                    loginViewModel!!.submitLoginDetail(userNameValue, passwordValue)
                } else {
                    Toast.makeText(context, context.getString(R.string.check_internet), Toast.LENGTH_SHORT).show()
                }
            }
        )

        VerticalSpacer(height = 30)

        if (!isLoading) CustomButton(
            enabled = !isUserNameError && !isPasswordError && userNameValue.isNotBlank() && passwordValue.isNotBlank(),
            text = context.getString(R.string.login),
            modifier = Modifier
                .padding(top = 16.dp)
                .height(50.dp)
                .fillMaxWidth()
                .align(Alignment.End),
            corner = RoundedCornerShape(5.dp),
            onClick = {
                keyboardController?.hide()
                if (isInternetAvailable(context)) {
                    loginViewModel!!.submitLoginDetail(userNameValue, passwordValue)
                } else {
                    Toast.makeText(context, context.getString(R.string.check_internet), Toast.LENGTH_SHORT).show()
                }
            }
        ) else CustomLoader()
    }
}


@Preview(showBackground = true, showSystemUi = true)
@Composable
fun GreetingPreview() {
    HomeWorkMVVMTheme {
        LoginCompose(null)
    }
}

private fun isInternetAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    val networkCapabilities = connectivityManager?.activeNetwork ?: return false
    val actNw = connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
    return when {
        actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
        actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
        actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
        else -> false
    }
}