package com.practical.homeworkmvvm.ui.login

import androidx.lifecycle.viewModelScope
import com.practical.homeworkmvvm.data.remote.login.response.LoginResponse
import com.practical.homeworkmvvm.data.remote.login.response.UserResponse
import com.practical.homeworkmvvm.domain.LoginUseCases
import com.practical.homeworkmvvm.ui.base.BaseViewModel
import com.practical.homeworkmvvm.utils.RequestParams
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class LoginViewModel(
    private val loginUseCases: LoginUseCases
) : BaseViewModel() {
    //    private val loginUseCases: LoginUseCases by inject()
    private var _loginResultLiveData = MutableStateFlow<LoginResponse?>(null)
    private var _isLoading = MutableStateFlow<Boolean>(false)
    val isLoading: StateFlow<Boolean>
        get() {
            return _isLoading
        }

    val resultLiveData: StateFlow<LoginResponse?>
        get() {
            return _loginResultLiveData
        }

    fun submitLoginDetail(
        userName: String, password: String
    ) {
        val requestParams = HashMap<String, Any>()
        requestParams[RequestParams.USERNAME] = userName
        requestParams[RequestParams.PASSWORD] = password
        viewModelScope.launch {
            loginUseCases.submitUserData(requestParams).onStart {
                _isLoading.value = true
            }.catch {
                _isLoading.value = false
            }.collect { loginResponse ->
                _isLoading.value = false
                _loginResultLiveData.value = loginResponse
            }
        }
    }

    fun insertUserData(
        userId: String, userName: String, isDeleted: Boolean
    ) {
        val userResponse = UserResponse(
            userId = userId, userName = userName, isDeleted = isDeleted
        )
        viewModelScope.launch(Dispatchers.IO) {
            loginUseCases.insertUserData(userResponse)
        }
    }
}