package com.practical.homeworkmvvm.ui.base

import androidx.lifecycle.ViewModel
import com.practical.homeworkmvvm.utils.LOG_TYPE_INFO
import com.practical.homeworkmvvm.utils.printLog
import org.koin.core.component.KoinComponent

abstract class BaseViewModel : ViewModel(), KoinComponent {

    init {
        javaClass.simpleName.printLog(LOG_TYPE_INFO, "created")
    }

    override fun onCleared() {
        super.onCleared()
        javaClass.simpleName.printLog(LOG_TYPE_INFO, "destroyed")
    }
}