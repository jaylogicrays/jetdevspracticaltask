package com.practical.homeworkmvvm.ui.base

import android.app.Application
import com.practical.homeworkmvvm.coin.apiModules
import com.practical.homeworkmvvm.coin.databaseModule
import com.practical.homeworkmvvm.coin.netModules
import com.practical.homeworkmvvm.coin.repositoryModules
import com.practical.homeworkmvvm.coin.userCases
import com.practical.homeworkmvvm.coin.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class IApp : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@IApp)
            modules(
                databaseModule, netModules, apiModules, repositoryModules, viewModelModules, userCases
            )
        }
    }
}